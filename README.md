urlshorterner
=============

How to run locally:
Install docker
Run docker-compose up -d
If you happen to already be using port 80 on your computer you'll have to manually edit the docker-compose file, replacing the line 80:80 with <desired_port>:80

After the service is already running (run docker-compose logs to check) run ```make init```

If you want to run the tests run make docker-test (make sure the docker container is running and has been initialised beforehand).

### Solution implementation
Technologies used: php + symfony3 + mysql
The authentication for the user was implemented using basic http auth.
The solution code can all be found at src/Appbundle/.
The tests are under the tests/ directory.
The user authentication logic is performed by the framework behind the scenes and is configured in the file app/config/security.yml


### Api documentation
If your docker server is up you can check the docs out at localhost/docs. Otherwise you can access them at http://46.101.3.44/docs (if the server is still up and running).


### Scalability of the solution:
One of the main requirements was that it was compatible with Debian, Ubuntu or Fedora, which eliminated options such as aws lambda + dynamodb, which are relatively easy to scale as they are designed for that.
That said, I used the php + mysql.
Assuming we want to scale for reads I can think of two options:
 1.Use a reverse proxy in front of the server, such as nginx or varnish to cache the GET requests, this could easily save a lot of hits to it.
 2.Use multiple slave mysql servers for reads. This solution would also require using a load balancer to distribute the load between an arbitrary number (Using something like AWS ELB + EC2, which makes it automatic to add and remove frontend servers).
 
 If it was required to scale for writes then I might try to look into using a solution such as redis-cluster or implement some kind of mysql sharding.


### Extra task
The extra task "Create a deployment script and deploy it somewhere." has been implemented using terraform and deployed using a docker container.

The current implementation is done in such a way that if we want to redeploy we need to destroy the whole infrastructure and recreate it. With more time I would have implemented a deploy script that doesn't need to destroy the infrastructure.

The code used to create the infrastructure + deploy is in terraform/digitalocean.tf
The containers definition can be found in docker-compose.yml + php7-fpm/

The server is deployed to 46.101.3.44

If you want to test it you'll need:
- a digital ocean access token
- setting up your digital ocean account with your private key
- your ssh key fingerprint
- terraform (https://www.terraform.io/downloads.html)
- run:
```
cd terraform
terraform init
terraform apply
```
terraform apply will ask you for your digitalocean token (do_token) and your ssh_fingerprint (ssh_fingerprint).
To obtain your ssh fingerprint you can run if you're on debian stretch: ``` ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub | awk '{print $2}' | sed -n 's/MD5://p' ```

### If I'd had more time...
- Occasionally the server will return an error when two shortened urls have the same length, this could have been addressed with retry logic with more time.
- Most logic is in the controller, with more time I would have moved more logic to the repositories and made sure it is unit tested (and not just functionally tested).
- I'm aware the server is deployed in dev mode with more time I would have deployed it in "production mode", which is more performant and doesn't give potential attackers as much data about the server
- I might have chosen a different server technology such as Go, which is more performant but might have required me to write more code to be able to produce this kind of service, as it does not have as rich an ecosystem compared to php/symfony.

### Other comments:
- Users don't need to be authenticacated to shorten URLs
- we assume the domain name is shr.ly
- Users short urls can be queried by anyone
 
