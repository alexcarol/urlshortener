<?php

namespace Tests\Functional\AppBundle\Controller;

use AppBundle\Entity\URLData;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ApiControllerTest extends WebTestCase
{
    protected function setUp()
    {
        $doctrine = static::createClient()->getContainer()->get('doctrine');
        $repository = $doctrine->getRepository(URLData::class);
        $repository->deleteAll();

        $userRepository = $doctrine->getRepository(User::class);
        $userRepository->deleteAll();

        $doctrine->getManager()->flush();
    }

    public function testCreateURLChosenByUser()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/v1/shorturl/ggl', [
            'original_url' => 'google.com',
        ]);

        $response1 = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response1->getStatusCode());

        $client->request('GET', '/api/v1/shorturl/ggl/original');

        $response2 = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response2->getStatusCode());

        $this->assertEquals('google.com', json_decode($response2->getContent(), true));
    }

    public function testUserProvidedURLWithMoreThan16CharactersIsRejected()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/v1/shorturl/' . str_repeat('a', 17), [
            'original_url' => 'google.com',
        ]);

        $response1 = $client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response1->getStatusCode());
    }

    public function testCreateURLChosenByUserAlreadyExists()
    {
        $client = static::createClient();
        $client->request('PUT', '/api/v1/shorturl/ggl', [
            'original_url' => 'google.com',
        ]);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());

        $client->request('PUT', '/api/v1/shorturl/ggl', [
            'original_url' => 'facebook.com',
        ]);
        $this->assertEquals(Response::HTTP_CONFLICT, $client->getResponse()->getStatusCode());
    }

    public function testCreateShortURLProgramatically()
    {
        // arrange
        $client = static::createClient();

        $client->request('POST', '/api/v1/shorturl', [
            'original_url' => 'facebook.com',
        ]);
        $response1 = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response1->getStatusCode(), $response1->getContent());

        $location = $response1->headers->get('Location');
        $prefix = '/api/v1/shorturl/';
        $this->assertStringMatchesFormat($prefix . '%s', $location);

        $shortURLPath = substr($location, strlen($prefix));
        $this->assertLessThanOrEqual(16, strlen($shortURLPath));

        $client->request('GET', $location . '/original');

        $response2 = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response2->getStatusCode(), $response2->getContent());

        $expectedContent = 'facebook.com';
        $this->assertEquals($expectedContent, json_decode($response2->getContent(), true));
    }

    public function testGettingMetadata()
    {
        $client = static::createClient();

        $client->request('POST', '/api/v1/shorturl', [
            'original_url' => 'facebook.com',
        ]);
        $response1 = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response1->getStatusCode(), $response1->getContent());

        $location = $response1->headers->get('Location');
        $this->assertStringMatchesFormat('/api/v1/shorturl/%s', $location);
        $client->request('GET', $location);

        $response2 = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response2->getStatusCode());

        $shortenedURLData = json_decode($response2->getContent(), true);

        $this->assertArrayHasKey('original_url', $shortenedURLData);
        $this->assertArrayHasKey('creation_time', $shortenedURLData);
        $this->assertArrayHasKey('times_accessed', $shortenedURLData);

        $this->assertEquals('facebook.com', $shortenedURLData['original_url']);
        $this->assertEquals(0, $shortenedURLData['times_accessed']);

        $creationTime = $shortenedURLData['creation_time'];

        $this->assertGreaterThanOrEqual(time() - 2, $creationTime);
        $this->assertLessThanOrEqual(time(), $creationTime);
    }

    public function testGetUserCreatedURLs()
    {
        $client = static::createClient();

        // url created by random user
        $client->request('PUT', '/api/v1/shorturl/abcd', [
            'original_url' => 'abc.com',
        ]);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());


        // create user
        $client->request('PUT', '/api/v1/user/alex', [
            'password' => 'alexpass',
        ]);

        $createUserResponse = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $createUserResponse->getStatusCode());

        $client->request(
            'PUT',
            '/api/v1/shorturl/ggl',
            [
                'original_url' => 'google.com',
            ],
            [],
            [
                'PHP_AUTH_USER' => 'alex',
                'PHP_AUTH_PW' => 'alexpass',
            ]
        );
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());


        $client->request('GET', '/api/v1/user/alex/shorturl');

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $userData = json_decode($response->getContent(), true);

        $this->assertCount(1, $userData);

        $shortenedURLData = $userData[0];

        $this->assertArrayHasKey('original_url', $shortenedURLData);
        $this->assertArrayHasKey('short_url', $shortenedURLData);
        $this->assertArrayHasKey('creation_time', $shortenedURLData);
        $this->assertArrayHasKey('times_accessed', $shortenedURLData);

        $this->assertEquals('google.com', $shortenedURLData['original_url']);
        $this->assertEquals('shr.ly/ggl', $shortenedURLData['short_url']);
        $this->assertEquals(0, $shortenedURLData['times_accessed']);

        $creationTime = $shortenedURLData['creation_time'];

        $this->assertGreaterThanOrEqual(time() - 2, $creationTime);
        $this->assertLessThanOrEqual(time(), $creationTime);
    }

    public function testCreateTheSameUserTwiceFails()
    {
        $client = static::createClient();

        $client->request('PUT', '/api/v1/user/alex', [
            'password' => 'alexpass',
        ]);

        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());

        $client->request('PUT', '/api/v1/user/alex', [
            'password' => 'abcd',
        ]);

        $this->assertEquals(Response::HTTP_CONFLICT, $client->getResponse()->getStatusCode());
    }

    public function testQueryingForNonexistentUserReturns404()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/user/alex/shorturl');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    public function testQueryingForNonexistentUrlReturns404()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/shorturl/a12345');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());

        $client->request('GET', '/api/v1/shorturl/a12345/original');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }
}
