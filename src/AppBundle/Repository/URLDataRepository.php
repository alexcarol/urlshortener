<?php

namespace AppBundle\Repository;

class URLDataRepository extends \Doctrine\ORM\EntityRepository
{
    public function deleteAll(): void
    {
        $entities = $this->findAll();

        foreach ($entities as $entity) {
            $this->getEntityManager()->remove($entity);
        }
    }
}
