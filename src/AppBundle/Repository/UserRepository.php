<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function deleteAll(): void
    {
        $entities = $this->findAll();

        foreach ($entities as $entity) {
            $this->getEntityManager()->remove($entity);
        }
    }
}
