<?php

namespace AppBundle\Controller;

use AppBundle\Entity\URLData;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * @Route("/api/v1")
 */
class ApiController extends Controller
{
    private const SHORT_PREFIX = 'shr.ly/';
    private const MAX_LENGTH = 16;

    private $entityManager;
    private $router;
    private $passwordEncoder;

    public function __construct(EntityManager $entityManager, Router $router, PasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Rest\Put("/user/{username}")
     * @RequestParam(name="password")
     * @SWG\Parameter(
     *     name="username",
     *     in="path",
     *     type="string",
     *     description="User name"
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     type="string",
     *     description="Desired password"
     * )
     * @SWG\Response(
     *     response=204,
     *     description="User was created successfully"
     * )
     * @SWG\Response(
     *     response=409,
     *     description="Conflict, user already exists"
     * )
     */
    public function createUser(string $username, ParamFetcher $paramFetcher)
    {
        $userRepository = $this->entityManager->getRepository(User::class);
        if ($userRepository->findOneBy(['username' =>  $username]) !== null) {
            throw new ConflictHttpException('User already exists');
        }

        $rawPassword = $paramFetcher->get('password');

        $encryptedPassword = $this->passwordEncoder->encodePassword($rawPassword, '');

        $user = new User($username, $encryptedPassword);

        $this->entityManager->persist($user);

        $this->entityManager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Get("/user/{username}/shorturl", name="user_short_url_info")
     * @SWG\Parameter(
     *     name="username",
     *     in="path",
     *     type="string",
     *     description="User name"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns a list of the URLs the user created with all their information"
     * )
     * @SWG\Response(
     *     response=404,
     *     description="User not found"
     * )
     */
    public function getUserUrls(string $username)
    {
        $user = $this
            ->entityManager
            ->getRepository(User::class)
            ->findOneBy(['username' => $username])
        ;

        if (!$user instanceof User) {
            throw new  NotFoundHttpException();
        }

        $urls = $user->getURLs();

        return new  JsonResponse(array_map(
            function(URLData $data) {
                return [
                    'original_url' => $data->getOriginalURL(),
                    'short_url' => self::SHORT_PREFIX . $data->getShortURL(),
                    'creation_time' => $data->getCreationTimestamp(),
                    'times_accessed' => $data->getTimesAccessed(),
                ];
            },
            $urls
        ));
    }

    /**
     * @Rest\Get("/shorturl/{shortURL}/original")
     * @SWG\Parameter(
     *     name="shortURL",
     *     in="path",
     *     type="string",
     *     description="Short url"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the original url"
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Short url not found"
     * )
     */
    public function getOriginalURL(string $shortURL)
    {
        $data = $this->getDataFromShortURL($shortURL);

        if  (!$data instanceof URLData) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($data->getOriginalURL());
    }

    /**
     * @Rest\Get("/shorturl/{shortURL}", name="short_url_info")
     * @SWG\Parameter(
     *     name="shortURL",
     *     in="path",
     *     type="string",
     *     description="Short url"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns all the short url information"
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Short url not found"
     * )
     */
    public function getShortURLInfo(string $shortURL)
    {
        $data = $this->getDataFromShortURL($shortURL);

        if  (!$data instanceof URLData) {
            throw new NotFoundHttpException();
        }

        return new  JsonResponse([
            'original_url' => $data->getOriginalURL(),
            'creation_time' => $data->getCreationTimestamp(),
            'times_accessed' => $data->getTimesAccessed(),
        ]);
    }

    /**
     * @Rest\Post("/shorturl")
     * @RequestParam(name="original_url")
     *
     * @SWG\Parameter(
     *     name="original_url",
     *     in="formData",
     *     type="string",
     *     description="URL that is meant to be shortened"
     * )
     *
     * @SWG\Parameter(
     *     name="PHP_AUTH_USER",
     *     in="header",
     *     type="string",
     *     description="User name",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="PHP_AUTH_PW",
     *     in="header",
     *     type="string",
     *     description="User password",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="The url has been shortened successfully"
     * )
     */
    public function createShortURL(ParamFetcher $paramFetcher)
    {
        $shortURL = $this->randString();

        $user = $this->getUser();
        $originalURL = $paramFetcher->get('original_url');

        $data = new URLData($shortURL, $originalURL, time(), $user);

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        $route = $this->router->generate('short_url_info', ['shortURL' => $shortURL]);

        return new Response('', Response::HTTP_CREATED, ['Location' => $route]);
    }

    function randString(): string
    {
        $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $randomString = '';
        $lastElementPosition = strlen($charset) - 1;
        for ($i = 0; $i < self::MAX_LENGTH; ++$i) {
            $randomString .= $charset[random_int(0, $lastElementPosition)];
        }

        return $randomString;
    }

    /**
     * @Rest\Put("/shorturl/{shortURL}")
     * @RequestParam(
     *  name="original_url"
     * )
     *
     * @SWG\Parameter(
     *     name="shortURL",
     *     in="path",
     *     type="string",
     *     description="Desired short url, length can't be longer than 16 characters"
     * )
     *
     * @SWG\Parameter(
     *     name="original_url",
     *     in="formData",
     *     type="string",
     *     description="URL that is meant to be shortened"
     * )
     *
     * @SWG\Parameter(
     *     name="PHP_AUTH_USER",
     *     in="header",
     *     type="string",
     *     description="User name",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="PHP_AUTH_PW",
     *     in="header",
     *     type="string",
     *     description="User password",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="The url has been shortened successfully"
     * )
     * @SWG\Response(
     *     response=409,
     *     description="The shortened URL already exists"
     * )
     */
    public function createShortURLFromUserProvidedURL(string $shortURL, ParamFetcher $paramFetcher)
    {
        if (strlen($shortURL) > self::MAX_LENGTH) {
            throw new BadRequestHttpException('Short URLs can\'t have more than 16 characters');
        }

        $user = $this->getUser();

        $urlDataRepository = $this->entityManager->getRepository(URLData::class);
        if ($urlDataRepository->findOneBy(['shortURL' =>  $shortURL]) != null) {
            throw new ConflictHttpException('Short url already exists');
        }

        $originalURL = $paramFetcher->get('original_url');
        $data = new  URLData($shortURL, $originalURL, time(), $user);

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    private function getDataFromShortURL(string $shortURL): ?URLData
    {
        return $this->entityManager->getRepository(URLData::class)->findOneBy(['shortURL' => $shortURL]);
    }
}
