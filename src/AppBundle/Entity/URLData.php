<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * URLData.
 *
 * @ORM\Table(name="domain_model_url_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\URLDataRepository")
 */
class URLData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shortURL", type="string", length=23, unique=true)
     */
    private $shortURL;

    /**
     * @var string
     *
     * @ORM\Column(name="originalURL", type="string", length=255)
     */
    private $originalURL;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="creationTime", type="datetime_immutable")
     */
    private $creationTime;

    /**
     * @var int
     *
     * @ORM\Column(name="timesAccessed", type="integer")
     */
    private $timesAccessed = 0;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="urls")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct(string $shortURL, string $originalURL, int $creationTime, ?User $user)
    {
        $this->shortURL = $shortURL;
        $this->originalURL = $originalURL;
        $this->creationTime = \DateTimeImmutable::createFromFormat('U', $creationTime);
        $this->user = $user;
    }

    /**
     * Get shortURL.
     *
     * @return string
     */
    public function getShortURL()
    {
        return $this->shortURL;
    }

    public function getOriginalURL(): string
    {
        return $this->originalURL;
    }

    public function getCreationTimestamp(): int
    {
        return $this->creationTime->getTimestamp();
    }

    /**
     * Get timesAccessed.
     *
     * @return int
     */
    public function getTimesAccessed()
    {
        return $this->timesAccessed;
    }
}
