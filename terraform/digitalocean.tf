variable "do_token" {}
variable "ssh_fingerprint" {}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "hellodocker" {
  image  = "docker-16-04"
  name   = "hellodocker"
  region = "lon1"
  size   = "512mb"
  ssh_keys =  [
    "${var.ssh_fingerprint}"
  ]

  provisioner "remote-exec" {
    inline = [
      "git clone https://gitlab.com/alexcarol/urlshortener.git",
      "cd urlshortener",
      "docker-compose up -d",
      "sleep 120", // we should way for the healthcheck, but this is quick and dirty and just works (for now)
      "make docker-init"
    ]
    connection {
      user = "root"
      type = "ssh"
      timeout = "2m"
    }
  }
}
