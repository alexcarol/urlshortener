test:
	vendor/bin/phpunit

docker-test:
	docker-compose exec php make test

docker-init:
	docker-compose exec php bin/console doctrine:database:drop --force
	docker-compose exec php bin/console doctrine:database:create
	docker-comNpose exec php bin/console doctrine:schema:create